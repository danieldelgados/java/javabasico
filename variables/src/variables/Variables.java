package variables;

public class Variables {

	public static void main(String[] args) {
		int myNumber=88; //variable declarada e inicializada
		short myShort = 847;
		long myLong = 9797;
		
		double myDouble = 3.14;
		float myFloat = 324.3f;
		
		char myChar = 'y';
		boolean myBoolean = false;
		
		byte myByte = 127;
		
		System.out.println(myNumber);  
		System.out.println(myShort);
		System.out.println(myLong);
		System.out.println(myDouble);
		System.out.println(myFloat);
		System.out.println(myChar);
		System.out.println(myBoolean);
		System.out.println(myByte);
	}

}
