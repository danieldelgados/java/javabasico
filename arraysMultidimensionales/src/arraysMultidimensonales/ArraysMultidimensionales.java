package arraysMultidimensonales;

public class ArraysMultidimensionales {

	public static void main(String[] args) {
		
		int[] valores = {3,5,2343}; // array unidimensional
		
		System.out.println(valores[2]);
		
		int[][] grid = {    // array multidemnsional
				{3,5,2343},
				{2, 4},
				{1, 2, 3, 4}
		};
		
		System.out.println(grid[1][1]);
		System.out.println(grid[0][2]);
		
		String[][] texts = new String[2][3];
		
		System.out.println(texts[0][1]);
		
		texts[0][1] = "Hola";
		
		System.out.println(texts[0][1]);
		
		for(int fila=0; fila<grid.length; fila++) {
			for(int col=0; col<grid[fila].length; col++ ) {
				System.out.print(grid[fila][col] + " ");
			}
			System.out.println();
		}
		
		String[][] words = new String[2][];
		
		System.out.println(words[0]);
		
		words[0] = new String[3];
		words[1] = new String[1];
		
		words[0][0] = "Hola";
		words[0][1] = "mundo";
 		words[0][2] = "Qu� tal?";
 		
 		words[1][0] = "adios";
 		
 		System.out.println(words[0][0]);
 		
 		
 		for(int i = 0; i<words.length; i++) {
 			for(int k =0; k < words[i].length; k++) {
 			System.out.print(words[i][k] + " ");
 			}
 			System.out.println();
 		}
 		
	}

}
