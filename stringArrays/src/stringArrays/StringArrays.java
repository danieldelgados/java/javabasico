package stringArrays;

public class StringArrays {

	public static void main(String[] args) {
		
		String[] words = new String[3];
		
		words[0] = "Hello";
		words[1] = "world";
		words[2] = "how are you?";
		
		System.out.println(words[2]);// imprimo el tercer elemento del array con indice 2
		
		for(int i =0; i<words.length;i++) {
			System.out.println(words[i]);//imprimo todos los elementos del array word
		}
		
		String[] fruits = {"apple", "banana", "pear", "kiwi"};
		
		for(String fruit: fruits) {
			System.out.println(fruit);//otra forma de imprimir los elementos del array fruits
		}
		
		int value = 0;//tipo primitivo
		
		String text = null;//tipo no primitivo "clase"
		
		System.out.println(text);//null es una referencia especial no una cadena de caracteres
		
		String[] texts = new String[2];
		
		System.out.println(texts[0]);// imprime null
		
		texts[0] = "one";// asignamos un valor a al primer elemento con indice cero
		
		System.out.println(texts[0]);//imprime one
	}

}
