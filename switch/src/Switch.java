
import java.util.Scanner;

public class Switch {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		System.out.println("Escribe una orden: ");
		String text = input.nextLine();

		switch (text) {

		case "inicia":
			System.out.println("La maquina inicia");
			break;

		case "stop":
			System.out.println("La maquina se detiene");
			break;

		default:
			System.out.println("La orden no se reconoce");

		}

	}

}
