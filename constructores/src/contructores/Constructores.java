package contructores;

class Machine{
	
	private String name;
	private int code;
	
	public Machine() {
		this("Arnie",800);// aqui estamos invocando al tercer constructor porque es el unico que acepta 2 argumentos y Java lo busca automaticamente
		System.out.println("Primer constructor running");
	}
	
	public Machine(String name) {
		System.out.println("Segundo constructor running");
		this.name = name;
	}
	
	public Machine(String name, int code) {
		System.out.println("Tercer constructor running");
		this.name = name;
		this.code = code;
	}
}

public class Constructores {

	public static void main(String[] args) {

		Machine machine1 = new Machine();
		
		Machine machine2 = new Machine("Berto");
		
		Machine machine3 = new Machine("Lucky", 1000);

	}

}
