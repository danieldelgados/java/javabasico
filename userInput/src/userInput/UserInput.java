package userInput;

import java.util.Scanner;//

public class UserInput {

	public static void main(String[] args) {
		//Ingresar datos(input)
		
		//Creamos el Objeto Scanner
		Scanner input = new Scanner(System.in);// podemos llamar como queramos a la variable "input", nueces si quiero, es una variable!! y creamos un nuevo objeto
		
		//Output the prompt
		System.out.println("Escribe una linea de texto:");
		
		//El usuario escribir� algo(puedo pedir numero y ser�a el mismo resultado)
		String line = input.nextLine();
		//Aqui nos dice que escribi� el usuario
		System.out.println("Escribiste una la linea de texto: " + line);
	}

}
