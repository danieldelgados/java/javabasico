package arrays;

public class Arrays {

	public static void main(String[] args) {
		
		int valor = 7;// valores por el valor
		
		int[] valores;//valores por referencia
		
		valores = new int[3];
		
		valores[0]=10;
		valores[1]=20;
		valores[2]=30;
		
		System.out.println(valores[0]);
		System.out.println(valores[1]);
		System.out.println(valores[2]);
		
		for(int i=0;i<valores.length;i++) {
			System.out.println(valores[i]);
		}
		
		int[] numeros = {5,6,7};//la forma m�s comoda y comun de trabajar con arrays
		
		for(int i=0;i<numeros.length;i++) {
			System.out.println(numeros[i]);
		}
	}

}
