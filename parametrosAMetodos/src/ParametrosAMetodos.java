class Robot {
	public void hablar(String texto) {//aqui pasamos un parametro o argumento
		System.out.println(texto);
	}
	
	public void saltar(int altura) {
		System.out.println("Saltar: " + altura + " cm.");
	}
	
	public void mover(String direccion, double distancia) {
		System.out.println("Moviendome " + distancia + " mts en direcci�n " +direccion  );
	}
}

public class ParametrosAMetodos {

	public static void main(String[] args) {
		
		Robot robocop = new Robot();
		robocop.hablar("Hola mi nombre es Robocop.");
		robocop.saltar(20);
		robocop.mover("Este", 20.1);
		
		String saludo = "Hola, qu� tal?";
		
		robocop.hablar(saludo);
		
		int valor = 14;
		
		robocop.saltar(valor);

	}

}
