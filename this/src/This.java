class Frog{
	private String name;// con privado enforzamos el encapsulamiento, nadie puede alterar nuestras variables directamente
	private int age;
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setAge(int age) {
		this.age = age;
	}
	
	public String getName() {
		return name;
	}
	
	public int getAge() {
		return age;
	}
	
	public void setInfo(String name, int age) {
		setName(name);
		setAge(age);
	}
}

public class This {

	public static void main(String[] args) {
		
		Frog frog1 = new Frog();
		
		//frog1.name = "Berto"; esto es poco optimo porque tendr�a que saber siempre el nombre de las variables de una clase
		
		frog1.setName("Berto");//aqu� solo tengo que saber los metodos y pasar lo que quiero como parametros o argumentos
		
		//frog1.age = 31;
		frog1.setAge(31);
		
		System.out.println(frog1.getName());
		System.out.println(frog1.getAge());
	}

}
