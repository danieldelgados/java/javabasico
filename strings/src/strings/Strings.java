package strings;

public class Strings {
	public static void main(String[] args) {
		int myInt = 7;
		double myDouble = 7.8;
		
		String text = "Hello";
		String blank = " ";
		String name = "Daniel";
		String greeting = text + blank +name;
		
		
		
		System.out.println(text);
		
		System.out.println(greeting);
		
		System.out.println("Hello" + " " + "Bob");
		
		System.out.println("My integer is: " + myInt);
		
		System.out.println("My number is: " +myDouble+ ".");
	}
	
}
