package clasesYObjetos;

class Persona{
	// Las clases pueden contener 
	
	//1. Datos
	//2. sub rutinas (metodos)
	
	//Variables  (datos o estados)
	
	String name;
	int age;
	
	//subrutinas (metodos)
	
	void hablar() {
		for(int i =0; i<3; i++) {
		System.out.println("Hello mi nombre es: " + name + " tengo " + age + " a�os");
		}
	}
	
	void decirHola() {
		System.out.println("Hola!");
	}
}

public class ClasesYObjetos {

	public static void main(String[] args) {// sub rutina metodo principal
		//creamos un objeto de de la clase persona
		
		Persona persona1 = new Persona();
		
		persona1.name = "Daniel Delgado";
		persona1.age = 32;
		persona1.hablar();
		persona1.decirHola();
		
		//creamos otro objeto de la clase persona
		
		Persona persona2 = new Persona();
		
		persona2.name = "Adriana Smith";
		persona2.age = 26;
		persona2.hablar();
		persona2.decirHola();
		
		//Imprimimos algunas propiedades de las personas
		System.out.println(persona1.name);
		System.out.println(persona2.name);
		
		
	}

}
