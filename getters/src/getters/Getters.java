package getters;

class Persona {
	String nombre;
	int edad;
	
	void hablar() {
		System.out.println("Mi nombre es: " + nombre);
	}
	
	// metodos que devuelven datos
	int calcularJubilacion() {
		int a�osFaltantes = 65 - edad;
						
		return a�osFaltantes;
	}
	
	int getEdad() {
		return edad;
	}
	
	String getNombre() {
		return nombre;
	}
}

public class Getters {

	public static void main(String[] args) {
		Persona persona1 = new Persona();
		
		persona1.nombre = "Daniel";
		persona1.edad = 31;
		
		//persona1.hablar();
		persona1.calcularJubilacion();
		
		int a�os = persona1.calcularJubilacion();
		
		System.out.println("A�os que faltan para jubilar: " + a�os);
		
		int edad = persona1.getEdad();
		String nombre = persona1.getNombre();
		
		System.out.println("Nombre es: " + nombre);
		System.out.println("Edad es: " + edad);

	}

}
