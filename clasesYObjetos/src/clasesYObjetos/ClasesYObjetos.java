package clasesYObjetos;

class Persona{
	// Las clases pueden contener 
	//1. Datos
	//2. sub rutinas (metodos)
	
	//Variables (datos o estados)
	
	String name;
	int age;
	
}

public class ClasesYObjetos {

	public static void main(String[] args) {// sub rutina metodo principal
		
		Persona persona1 = new Persona();
		
		persona1.name = "Daniel Delgado";
		persona1.age = 32;
		
		Persona persona2 = new Persona();
		
		persona2.name = "Adriana Smith";
		persona2.age = 26;
		
		System.out.println(persona1.name);
		System.out.println(persona2.name);
	}

}
